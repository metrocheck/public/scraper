#!/usr/bin/env node
'use strict'

require('dotenv').config()
const meow = require('meow')
const fs = require('fs')
const logger = require('../src/logger')
const { fetchPoint } = require('../src/api')
const transformPoint = require('../src/transformers/point')
const { persistPoints } = require('../src/db')
const sleep = require('atomic-sleep')

const cli = meow(`
	Usage
	  $ fetch-points [--from-file FILE][<ids>...]

	Options
	  --from-file, -f <FILE> Path to a list of ids

	Examples
	  $ fetch-points 8254 8965 
	  🌈 saved file /tmp/file.csv 🌈
`, {
  flags: {
    'from-file': {
      type: 'string',
      alias: 'f'
    },
    force: {
      type: 'boolean'
    }
  }
})

async function main () {
  let ids; const points = []

  if (cli.input.length > 0) { // Ids provided on command line
    logger.debug('cli input provided', cli.input)
    ids = cli.input
  } else if (cli.flags.fromFile) { // Id file provided
    try {
      ids = fs.readFileSync(cli.flags.fromFile, { encoding: 'utf-8' }).split('\n').filter(s => !!s)
      logger.debug(`Reading ids from file ${cli.flags.fromFile}`)
    } catch (e) {
      logger.error('Failed to parse input file')
      logger.error(e.message)
      return
    }
  } else {
    logger.error('No ids provided')
    return
  }

  const waitTime = 3000
  logger.info(`${ids.length} ids found. Going to fetch one every ${waitTime}ms`)

  for (const id of ids) {
    const stibPoint = await fetchPoint(id)
    const metrocheckPoint = transformPoint(stibPoint)

    console.debug('.')
    points.push(metrocheckPoint)
    sleep(waitTime)
  }

  logger.info(`Fetched ${points.length} points.`)

  if (cli.flags.force) {
    logger.debug('Going to persist records')
    await persistPoints(points)
  }

  logger.info('Finishing run')
  return 0
}

main()
