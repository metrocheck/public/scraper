#!/usr/bin/env node
'use strict'

require('dotenv').config()
const meow = require('meow')
const moment = require('moment')
const logger = require('../src/logger')
const { fetchPositions } = require('../src/api')
const transformPosition = require('../src/transformers/position')
const { persistPositions } = require('../src/db')
const { fetchedLines } = require('../src/config/dev')

// eslint-disable-next-line no-tabs
const cli = meow(`
	Usage
	  $ fetch-positions [<lines>...]

	Options
    -- force Persist records in database

	Examples
	  $ fetch-positions 1 2 
`, {
  flags: {
    force: {
      type: 'boolean'
    }
  }
})

async function main () {
  let ids
  /** @var {MetrocheckPosition[]} */
  let positions = []

  if (cli.input.length > 0) { // lines provided on command line
    // logger.debug('cli input provided', cli.input)
    ids = cli.input
  } else {
    logger.debug('No line ids provided inline. Falling back to config value')
    ids = fetchedLines
  }

  const rawPositions = await fetchPositions(ids)
  rawPositions.forEach(line =>
    line.vehiclePositions.forEach(p => {
      positions = positions.concat(transformPosition(p, moment().format('YYYY-MM-DD HH:mm:ss'), line.lineId))
    })
  )

  // console.log(
  // positions,
  // rawPositions
  // rawPositions[0].vehiclePositions,
  // positions.map(p => p.point_fk)
  // )
  logger.debug(`Fetched ${positions.length} positions.`)

  if (cli.flags.force && positions.length) {
    logger.debug('Going to persist records')
    await persistPositions(positions)
  }

  logger.info(`${positions.length} positions processed`)
  return 0
}

main()
