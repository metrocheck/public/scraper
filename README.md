# Metrocheck scraper

Network data scraper.

Fetches, transform and store data.

## Usage
Two binary files are available. One fetches positions, the other points.

```shell script
# Fetch positions for lines 1, 2, 5 and 6, and stores result in DB
/wait && /bin/watch -n 20 'bin/fetch-positions.js 1 2 5 6 --force'

# Fetch points from Id list in file /user/src/app/bin/ids.txt and stores result in DB
/user/src/app/bin/fetch-points.js -f /user/src/app/bin/ids.txt --force
```

## Environment

These environment variables are required :

```
DB_HOST             # Db host
DB_USER             # Db user
DB_PASSWORD_FILE    # Name of the file containing the password as supplied by docker secret mgt
DB_DATABASE         # Db name
TOKEN_FILE          # Opendata API token
```

When running on docker, these additional variables can be provided :
```
WAIT_HOSTS          # Hosts to wait for (e.g db)
```
