const moment = require('moment')
const MetrocheckPosition = require('../model/metrocheck/Position')

/**
 *
 * @param {StibPosition} original
 * @param datetime
 * @param lineFk
 * @return MetrocheckPosition
 */
module.exports = function (original, datetime = moment().format('YYYY-MM-DD HH:mm:ss'), lineFk) {
  return new MetrocheckPosition(
    null,
    datetime,
    lineFk,
    original.directionId,
    original.distanceFromPoint,
    original.pointId
  )
}
