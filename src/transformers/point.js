const MetrocheckPoint = require('../model/metrocheck/Point')

/**
 *
 * @param {StibPoint} original
 * @return {MetrocheckPoint}
 */
module.exports = function (original) {
  const { gpsCoordinates: { latitude, longitude }, id, name: { nl, fr } } = original
  return new MetrocheckPoint(
    id,
    fr,
    nl,
    latitude,
    longitude
  )
}
