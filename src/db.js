const mysql = require('mysql')
const config = require('./config')(process.env.NODE_ENV)
const logger = require('./logger')

let _db = null

function db () {
  const { host, user, password, database } = config.db
  if (!_db) {
    logger.debug('Created Mysql connection instance')
    _db = mysql.createConnection({
      host,
      user,
      password,
      database
      // debug: true
    })
  }

  return _db
}

const escape = (strings, ...values) =>
  strings.map((string, index) =>
    // @fixme
    `${string}${values[index] ? mysql.escape(values[index]) : ''}`
    // `${string}${values[index] ? values[index] : ''}`
  ).join('')

module.exports = {
  /**
   *
   * @param {MetrocheckPoint[]} points
   * @return {Promise<Query>}
   */
  persistPoints: async function (points) {
    const connection = db()
    const query = 'INSERT INTO `point` (`id`, `name_fr`, `name_nl`, `geoloc`) VALUES ' +
      points.map(p => {
        const { id, nameFr, nameNl, geoloc: [x, y] } = p

        return escape`(${id}, ${nameFr}, ${nameNl}, ST_GeomFromText("POINT(${x} ${y})"))`
      }).join(', ')

    const response = await connection.query(query)
    connection.end()
    return response
  },

  /**
   *
   * @param {MetrocheckPosition[]} positions
   * @return {Promise<Query>}
   */
  persistPositions: async function (positions) {
    const connection = db()
    const query = 'INSERT INTO `position` (`datetime`, `line_fk`, `direction_fk`,  `distance_from_point`, `point_fk`) VALUES ' +
      positions.map(
        p => escape`(${p.datetime},${p.line_fk},${p.direction_fk},${p.distance_from_point ? 1 : '0'},${p.point_fk})`)
        .join(',')

    const response = await connection.query(query)
    connection.end()
    return response
  }
}
