const axios = require('axios')
const conf = require('./config')(process.env.NODE_ENV)
const logger = require('./logger')

async function fetchApi (url) {
  try {
    // noinspection UnnecessaryLocalVariableJS
    const response = await axios.get(url, {
      headers: {
        Accept: 'application / json',
        Authorization: `Bearer ${conf.api.token}`
      }
    })

    return response
  } catch (response) {
    logger.error(`Api request failed. Status ${response.response.status} ${response.response.statusText} calling ${response.config.url}. Body : ${response.body}`)
  }
}

module.exports = {
  /**
   *
   * @param {number} id
   * @return {Promise<StibPoint>}
   */
  fetchPoint: async function (id) {
    const url = conf.api.routes.point.replace('%s', id.toString(10))
    const response = await fetchApi(url)

    if (response.data.points.length > 1) {
      logger.warn(`WARN : point ${id} had several point records`)
    } else if (!response.data.points.length) {
      logger.error(`No data for point ${id}`)
    }

    return response.data.points[0]
  },

  /**
   *
   * @param {?number[]} lines
   * @return {Promise<{lineId: string; vehiclePositions: StibPosition[]}[]>}
   */
  fetchPositions: async function (lines) {
    const url = conf.api.routes.positions.replace('%s', lines.join(encodeURIComponent(',')))
    const response = await fetchApi(url)

    if (!response || !response.data.lines.length) {
      logger.debug('No data returned')
      return []
    }

    return response.data.lines
  }
}
