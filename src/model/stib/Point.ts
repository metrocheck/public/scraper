interface GpsCoordinates {
    latitude: number
    longitude: number
}

interface BilingualText {
    fr: string
    nl: string
}

interface StibPoint {
    gpsCoordinates: GpsCoordinates
    id: number
    name: BilingualText
}
