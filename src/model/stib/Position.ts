interface StibPosition {
    directionId: string
    distanceFromPoint: number,
    pointId: string
}
