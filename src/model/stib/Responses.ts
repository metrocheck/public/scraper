///<reference path="Point.ts"/>
///<reference path="Position.ts"/>

interface PointResponse {
    data?: { points?: StibPoint[] }
}

interface PositionResponse {
    data?: {
        lines: {
            lineId: string,
            vehiclePositions: StibPosition[]
        }[]
    }
}
