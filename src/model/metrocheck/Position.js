/* eslint-disable camelcase */
module.exports = class MetrocheckPosition {
  /**
   *
   * @param {number} id
   * @param {string} datetime
   * @param {number} line_fk
   * @param {number} direction_fk
   * @param {number} distance_from_point
   * @param {number} point_fk
   */
  constructor (id, datetime, line_fk, direction_fk, distance_from_point, point_fk) {
    this.id = id
    this.datetime = datetime
    this.line_fk = line_fk
    this.direction_fk = direction_fk
    this.distance_from_point = distance_from_point
    this.point_fk = point_fk
  }
}
