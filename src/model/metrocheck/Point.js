module.exports = class MetrocheckPoint {
  /**
   *
   * @param {number} id
   * @param {string} nameFr
   * @param {string} nameNl
   * @param {number} x
   * @param {number} y
   */
  constructor (id, nameFr, nameNl, x, y) {
    this.id = id
    this.nameFr = nameFr
    this.nameNl = nameNl
    this.geoloc = [x, y]
  }
}
