export interface MetrocheckPosition {
    id: number
    datetime: string
    line_fk: number
    direction_fk: number
    distance_from_point: number
    point_fk: number
}
