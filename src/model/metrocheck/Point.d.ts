export interface MetrocheckPoint {
    id: number;
    nameFr: string;
    nameNl: string;
    geoloc: number[];
}
