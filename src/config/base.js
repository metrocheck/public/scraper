const secrets = require('../secrets')

module.exports = {
  db: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: secrets.get(process.env.DB_PASSWORD_FILE),
    database: process.env.DB_DATABASE
  },

  api: {
    routes: {
      point: 'https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointDetail/%s',
      positions: 'https://opendata-api.stib-mivb.be/OperationMonitoring/4.0/VehiclePositionByLine/%s'
    },
    token: secrets.get(process.env.TOKEN_FILE)
  },

  fetchedLines: [1, 2, 5, 6],
  logDirectory: '/var/log/metrocheck/scraper'
}
