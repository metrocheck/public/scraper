const env = process.env.NODE_ENV || 'dev'
const baseConfig = require('./base')

module.exports = Object.assign(baseConfig, {

  env: env
})
